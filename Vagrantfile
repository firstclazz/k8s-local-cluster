# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://vagrantcloud.com/search.
  IMAGE_NAME = "generic/centos7"
  # Set up the particular VMs where N is the workers count. The final count
  # of VMs will be N+1 where first machine will be k8s-master
  # and others will be k8s-workers-N
  WORKERS_COUNT = 2
  # List of k8s worker machines
  WORKERS = []

  # Set up k8s worker nodes
  (1..WORKERS_COUNT).each do |machine_id|

    # Set up machines
    config.vm.define "k8s-worker-#{machine_id}" do |machine|
      machine.vm.box = IMAGE_NAME
      machine.vm.hostname = "k8s-worker-#{machine_id}"
      WORKERS << "k8s-worker-#{machine_id}"

      # Provider = Parallels
      machine.vm.provider :parallels do |prl|
        prl.name = "k8s-worker-#{machine_id}"
        prl.update_guest_tools = true
      end
      # Provider = VirtualBox
      machine.vm.provider :virtualbox do |vb|
        vb.name = "k8s-worker-#{machine_id}"
      end
    end
  end

  # Set up k8s master node
  config.vm.define "k8s-master" do |master|
    master.vm.box = IMAGE_NAME
    master.vm.hostname = "k8s-master"

    # Provider = Parallels
    master.vm.provider :parallels do |prl|
      prl.name = "k8s-master"
      prl.update_guest_tools = true
    end
    # Provider = VirtulaBox
    master.vm.provider :virtualbox do |vb|
      vb.name  = "k8s-master"
    end

    master.vm.synced_folder ".", "/vagrant"

    # Prepare private keys of workers
    SSH_PRIVATE_KEY_FILES  = {}
    master.vm.provision :shell, inline: "rm -rf /home/vagrant/.pkeys"
    WORKERS.each do |worker|
      master.vm.provision :shell, inline: "mkdir -p /home/vagrant/.pkeys && cp /vagrant/.vagrant/machines/#{worker}/*/private_key /home/vagrant/.pkeys/#{worker}_private_key && chown -R vagrant:vagrant /home/vagrant/.pkeys"
      SSH_PRIVATE_KEY_FILES[worker] = "ansible_ssh_private_key_file=/home/vagrant/.pkeys/#{worker}_private_key"
    end
    # Provision machines using ansible
    master.vm.provision :ansible_local do |ansible|
      ansible.limit = "all"
      ansible.playbook = "kube-cluster/playbook.yml"
      ansible.host_vars = SSH_PRIVATE_KEY_FILES
      ansible.groups = {
        "master" => ["k8s-master"],
        "workers" => WORKERS
      }
    end
  end
end
